
public class MStart {
    public static void main(String[] args) {
        System.out.println("live");
        try {
            for (String arg:args) {
                Class c = Class.forName(arg);

                Print p = (Print) c.newInstance();
                p.print();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
