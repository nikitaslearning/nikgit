package forest;

/**
 * Created by n.bashkin on 13.11.2017.
 */
class Spruce extends Tree {   //считает объём конуса

    Spruce(int crownHeight, int crownDiameter, String crownType) {
        this.crownHeight = crownDiameter;
        this.crownDiameter = crownDiameter;
        this.crownType = crownType;
    }

    @Override
    double getVolumeValue() {
        double volume;
        double baseArea = Math.PI * (crownDiameter * crownDiameter);
        volume = (baseArea * crownHeight) / 3;

        return volume;
    }

    @Override
    double getDarkAreaValue() {
        double darkArea = Math.PI * (crownDiameter * crownDiameter);
        return darkArea;
    }

    @Override
    String getType() {
        return  crownType;
    }

    @Override
    String getName() {
        String name = getClass().getSimpleName();
        return name;
    }

}
