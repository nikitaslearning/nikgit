package forest;

/**
 * Created by n.bashkin on 13.11.2017.
 */
abstract class Tree {

    public int crownHeight;
    int crownDiameter;
    String crownType;

    abstract double getDarkAreaValue();

    abstract double getVolumeValue();

    abstract String getType();

    abstract String getName();

}

