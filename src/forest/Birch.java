package forest;

/**
 * Created by n.bashkin on 13.11.2017.
 */
class Birch extends Tree {

    Birch(int crownHeight, int crownDiameter, String crownType) {
        this.crownHeight = crownDiameter;
        this.crownDiameter = crownDiameter;
        this.crownType = crownType;
    }


    @Override
    double getVolumeValue() { //считает объём шара
        double volume = (4 * Math.PI * Math.pow(crownDiameter, 3)) / 3;
        return volume;
    }


    @Override
    double getDarkAreaValue() {
        double darkArea = Math.PI * (crownDiameter * crownDiameter);
        return darkArea;
    }

    @Override
    String getType() {
        return crownType;
    }

    @Override
    String getName() {
        String name = getClass().getSimpleName();
        return name;
    }

}
