package forest;

/**
 * Created by n.bashkin on 13.11.2017.
 */
class Palm extends Tree {

    Palm(int crownHeight, int crownDiameter, String crownType) {
        this.crownHeight = crownDiameter;
        this.crownDiameter = crownDiameter;
        this.crownType = crownType;
    }


    @Override
    double getVolumeValue() {  //читает объём цилиндра
        double volume = (Math.PI * (crownDiameter * crownDiameter)) * crownHeight;
        return volume;
    }

    @Override
    double getDarkAreaValue() {
        double darkArea = Math.PI * (crownDiameter * crownDiameter);
        return darkArea;
    }

    @Override
    String getType() {
        return crownType;
    }

    @Override
    String getName() {
        String name = getClass().getSimpleName();
        return name;
    }

}
