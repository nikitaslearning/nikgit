package forest;

/**
 * Created by n.bashkin on 13.11.2017.
 */
public class Starter {
    public static void main(String[] args) {

        TreeList treelist = new TreeList();
        treelist.addTree(15, 15, "globularis");
        treelist.addTree(3, 6, "pyramidalis");
        treelist.addTree(7, 15, "umbraculifera");
        treelist.addTree(20, 5, "pyramidalis");


        // print Volume
        System.out.println("Volume is " + treelist.trees[2].getVolumeValue());
        // print Dark area
        System.out.println("Dark area is " + treelist.trees[2].getDarkAreaValue());
        // print Type
        System.out.println("Type is " + treelist.trees[2].getType());
        // print Name
        System.out.println("Name is " + treelist.trees[2].getName());


    }
}
