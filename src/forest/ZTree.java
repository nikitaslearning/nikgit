package forest;

/**
 * Created by n.bashkin on 14.11.2017.
 */
public class ZTree extends Tree {

    ZTree(int crownHeight, int crownDiameter, String crownType){
        this.crownHeight = crownDiameter;
        this.crownDiameter = crownDiameter;
        this.crownType = crownType;
    }

    @Override
    double getDarkAreaValue() {
        return 0;
    }

    @Override
    double getVolumeValue() {
        return 0;
    }

    @Override
    String getType() {
        return crownType;
    }

    @Override
    String getName() {
        String name = getClass().getSimpleName();
        return name;
    }
}
