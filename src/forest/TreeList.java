package forest;

/**
 * Created by n.bashkin on 13.11.2017.
 */
public class TreeList {


    Tree[] trees = new Tree[0];

    public Tree[] getTrees() {
        return trees;
    }

    public void addTree(int crownHeight, int crownDiameter, String crownType) {
        Tree tree;

        if (trees.length > 0) {
            Tree[] buffer = trees;
            trees = new Tree[buffer.length + 1];
            System.arraycopy(buffer, 0, trees, 0, buffer.length);
        } else {
            trees = new Tree[1];
        }

        switch (crownType) {
            case "pyramidalis":
                tree = new Spruce(crownHeight, crownDiameter, crownType);
                trees[trees.length - 1] = tree;
                break;

            case "globularis":
                tree = new Birch(crownHeight, crownDiameter, crownType);
                trees[trees.length - 1] = tree;
                break;

            case "umbraculifera":
                tree = new Palm(crownHeight, crownDiameter, crownType);
                trees[trees.length - 1] = tree;
                break;

                case "Z-style":
                tree = new ZTree(crownHeight, crownDiameter, crownType);
                trees[trees.length - 1] = tree;
                break;

            default:
                tree = new Palm(crownHeight, crownDiameter, crownType);
                trees[trees.length - 1] = tree;
                break;
        }

    }
}
