package forestV2;

public class BallTree extends Tree {

    int width;

    BallTree(int width, String name){
        this.width = width;
        this.name = name;
    }


    @Override
    double getDarkAreaValue() { // считает площадь окружности
        int r = width/2;
        double darkArea = Math.PI *(r * r);
        return darkArea;
    }

    @Override
    double getVolumeValue() {// считает объём шара
        int r = width/2;
        double volume = (4*Math.PI*Math.pow(r,3))/3;
        return volume;
    }
}