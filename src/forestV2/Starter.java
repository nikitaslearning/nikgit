package forestV2;


import java.util.ArrayList;

public class Starter {
    public static void main(String[] args) {

        ArrayList<Tree> list = new ArrayList();
        list.add(new CubeTree(15,"ct1"));
        list.add(new ParalepipedTree(11,1,2,"pt1"));
        list.add(new BallTree(77,"bt1"));
        list.add(new ParalepipedTree(2,3,7,"pt2"));
//        list.add(new ZTree(5,5,2,2, "Zteree"));


        for(Tree tree : list) {
            System.out.println(tree.getName());
            System.out.println("Dark Value is: " + tree.getDarkAreaValue());
            System.out.println("Volume is: " + tree.getVolumeValue());
        }
    }
}