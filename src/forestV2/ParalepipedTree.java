package forestV2;


public class ParalepipedTree extends Tree {

    int width;
    int height;
    int depth;

    ParalepipedTree(int width, int height, int depth, String name){

        this.width = width;
        this.height = height;
        this.depth = depth;
        this.name = name;
    }

    @Override
    double getDarkAreaValue() { // считает объём паралепипеда
        int darkArea = width * depth * height;
        return darkArea;
    }

    @Override
    double getVolumeValue() { // считает площадь основания паралепипеда
        int volume = width * depth;
        return volume;
    }
}
