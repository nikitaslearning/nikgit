package forestV2;


abstract class Tree {
    String name;

    abstract double getDarkAreaValue();

    abstract double getVolumeValue();

    String getName() {
        String name = getClass().getSimpleName();
        return name;
    }

}