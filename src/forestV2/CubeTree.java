package forestV2;


class CubeTree extends Tree {


    int width;

    CubeTree(int width, String name) {
        this.width = width;
        this.name = name;
    }


    @Override
    double getVolumeValue() { //считает объём куба
        double volume = width * width * width;
        return volume;
    }


    @Override
    double getDarkAreaValue() {  //считает площадь основания куба
        double darkArea = width * width;
        return darkArea;
    }
}
