package Learning.lab2.pkg1;

public class SimpleParser extends ComLineParser {

    private String inFile, outFile;

    public SimpleParser(String[] keys) {
        super(keys);
    }

    public String getInFile() {
        return inFile;
    }

    public String getOutFile() {
        return outFile;
    }

    @Override
    protected void OnUsage(String errorKey) {
        if (errorKey != null) {
            System.out.println("Command-line switch error:" + errorKey);
        }
        System.out.println("формат ком.строки: имяПрограммы [-r<input-fileName>] [-w<output-fileName>]");
        System.out.println("   -?  показать Help файл");
        System.out.println("   -r  задать имя входного файла");
        System.out.println("   -w  выполнить вывод в указанный файл");
    }

    @Override
    protected SwitchStatus OnSwitch(String key, String keyValue) {
        SwitchStatus status = SwitchStatus.NoError;

        switch (key) {
            case "?":
                status = SwitchStatus.ShowUsage;
                break;
            case "r":
                if (keyValue != null) {
                    inFile = keyValue;
                } else {
                    System.out.println("Error. Value is null!");
                    status = SwitchStatus.Error;
                }
                break;
            case "w":
                if (keyValue != null) {
                    outFile = keyValue;
                } else {
                    System.out.println("Error. Value is null!");
                    status = SwitchStatus.Error;
                }
                break;
            default:
                System.out.println("no matches");
                System.out.println("SwitchStatus = Error");
        }

        return status;
    }

}
