package archive;

import java.util.Scanner;

/**
 * Created by Чёрнаямолния on 12.03.2017.
 */
public class DogsClub {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        boolean exit = false;
        OwnerManager ownerManager = new OwnerManager();
        DogManager dogManager = new DogManager();

        ownerManager.owners[0] = new Owner("karl", 514583);
        ownerManager.owners[1] = new Owner("semen", 887799);
        ownerManager.owners[2] = new Owner("fill", 123456);

        dogManager.dogs[0] = new Dog(12, 7, 4, "Bob");
        dogManager.dogs[1] = new Dog(15, 20, 10, "Tom");
        dogManager.dogs[2] = new Dog(2, 3, 1, "Silvia");
        dogManager.dogs[3] = new Dog(5, 3, 2, "Garry");

        ownerManager.owners[0].dogs[0] = dogManager.dogs[0];
        ownerManager.owners[1].dogs[0] = dogManager.dogs[1];
      //  ownerManager.owners[2].cats[0] = dogManager.cats[2];

        do {
            exit = true;
            System.out.println("1.Управление владельцами");
            System.out.println("2.Управление собаками");
            System.out.println("3.Закрепить собаку за владельцем");
            System.out.println("4.Вывести список принадлежности хозяин-собаки");
            System.out.println("5.Выход");
            int reaction = in.nextInt();
//Управление владельцами----------------------------------------------------------------------
            if (reaction == 1) {
                ownerManager.console();
                exit = false;
            }
//Управление собаками-------------------------------------------------------------------------
            if (reaction == 2) {
                dogManager.console();
                exit = false;
            }
//Закрепляем собак за владельцем--------------------------------------------------------------

            if (reaction == 3) {
                if (ownerManager.owners[0] == null) {
                    System.out.println("Нет ни одного владельца");
                } else {
                    for (int i = 0; i < ownerManager.owners.length; i++) {
                        System.out.println(i + 1 + ". " + ownerManager.owners[i].name);
                        if (ownerManager.owners[i + 1] == null) {
                            break;
                        }
                    }
                    System.out.println();
                    System.out.println("   Выбирите владельца собаке");
                }
                int picOwner = in.nextInt() - 1;
                for (int i = 0; i < dogManager.dogs.length; i++) {
                    System.out.println(i + 1 + ". " + dogManager.dogs[i].name);
                    if (dogManager.dogs[i + 1] == null) {
                        break;
                    }
                }
                System.out.println();
                System.out.println("   Выбирите собаку");
                int picDog = in.nextInt() - 1;

// Проверка принадлежности собак-------------------------------------------------------------
// проверка по массиву собак каждого хозяина

                boolean quit = true;
                for (int i = 0; i < ownerManager.owners.length; i++) {
                    if (ownerManager.owners[i + 1] == null) {
                        break;

//тут что то со скобками после ЭЛС
//    проверить позднее
                    } else
                        for (int i1 = 0; i1 < ownerManager.owners[i].dogs.length; i1++) {
                            if (dogManager.dogs[i1 + 1] == null) {
                                break;
                            } else if (dogManager.dogs[picDog] == ownerManager.owners[i].dogs[i1]) {
                                System.out.println("Собака принадлежит " + ownerManager.owners[i].name);
                                quit = false;
                            }
                        }
                }

                if (quit == true) {
                    ownerManager.owners[picOwner].addDog(dogManager.dogs[picDog]);

                    System.out.println(ownerManager.owners[picOwner].name + " теперь хзяин " + dogManager.dogs[picDog].name);
                }

                exit = false;

            }
//Список принадлежности хозяин-собаки--------------------------------------------------------
//ДОДЕЛАТЬ!!!!!!!!!!!!
// Не работает! Подумать над вариантом решения с помощью Вайл
// Доделать свободных собак
            if (reaction == 4) {
                for (int s = 0; s < ownerManager.owners.length; s++) {
                    if (ownerManager.owners[0] == null) {
                        System.out.println("Нет ниодного владельца");
                        break;
                    } else {
                        if (ownerManager.owners[s].dogs[0] == null) {
                            System.out.println(ownerManager.owners[s].name + " не имеет собак");
                        } else {
                            System.out.println(ownerManager.owners[s].name + " является владельцем:");
                            for (int s1 = 0; s1 < ownerManager.owners[s].dogs.length; s1++) {
                                System.out.println("- " + ownerManager.owners[s].dogs[s1].name);
                                if (ownerManager.owners[s].dogs[s1 + 1] == null) {
                                    break;
                                }
                            }
                        }
                        if (ownerManager.owners[s + 1] == null) {
                            break;
                        }
                    }
                }
                System.out.println("----------------");
                System.out.println("Свободные собаки:");
                String dogRes = "Нет ни обной собаки";
                if (dogManager.dogs.length == 0) {
                    System.out.println(dogRes);
                } else {
                    for (int d = 0; d < dogManager.dogs.length; d++) {
                        for (int d1 = 0; d1 < ownerManager.owners.length; d1++) {
                            for (int d2 = 0; d2 < ownerManager.owners[d1].dogs.length; d2++) {
                                dogRes = ownerManager.owners[d1].dogs[d2].name;
                                if (dogManager.dogs[d] == ownerManager.owners[d1].dogs[d2]) {
                                    dogRes = "Нет ни обной собаки";
                                    d++;
                                    break;
                                }
                                if (ownerManager.owners[d1].dogs[d2 + 1] == null) {
                                    break;
                                }
                            }
                            if (ownerManager.owners[d1 + 1] == null) {
                                break;
                            }
                        }
                        if (dogManager.dogs[d + 1] == null) {
                            break;
                        }
                        System.out.println(dogRes);
                    }
                    if (dogRes == "Нет ни обной собаки") {
                        System.out.println(dogRes);
                    }
                }
                exit = false;
            }


//Выход---------------------------------------------------------------------------------------
            if (reaction == 5) {
                exit = true;
            }
//Ошибка--------------------------------------------------------------------------------------
            if (reaction > 5 || reaction < 1) {
                System.out.println("Введите правильно пункт меню!");
                exit = false;
            }


        } while (exit == false);

//        System.out.println(ownerManager.owners[0].cats[0].name);
//        System.out.println(ownerManager.owners[0].cats[1].name);
//        System.out.println(ownerManager.owners[0].cats[2].name);





/*        archive.CatManager dogManager = new archive.CatManager();
        dogManager.console();
        owners[0].addCat(dogManager.cats[1]);
        owners[1].addCat(dogManager.cats[0]);
        owners[2].addCat(dogManager.cats[3]);
        owners[2].addCat(dogManager.cats[2]);

        for (int i = 0; i < owners.length; i++) {
            System.out.println(owners[i].ownerName);
            System.out.println();

            for (int i1 = 0; i1 < owners[i].dogCount; i1++) {
                archive.Cat res = owners[i].cats[i1];
                System.out.println("   " + res.name);
            }
            if (owners[i + 1] == null) {
                break;
            }
        }

**/
    }

}
