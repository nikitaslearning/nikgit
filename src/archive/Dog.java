package archive;

/**
 * Created by Чёрнаямолния on 21.02.2017.
 */
public class Dog {

    int length, weight, height;
    String name;

    Dog(int length, int weight, int height, String name) {
        this.length = length;
        this.weight = weight;
        this.height = height;
        this.name = name;

    }

    public int volume() {
        int s;
        s = this.length * this.weight * this.height;
        return s;
    }

}
