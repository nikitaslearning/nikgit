package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 * <p>
 * Задача 12
 * дан массив имен учеников
 * "Вася1"
 * "Вася2"
 * "Зина"
 * <p>
 * сделать отдельные списки :
 * 1. каждый третий ученик
 * 2. каждый пятый ученик
 * <p>
 * и вывести их
 */
public class Start12 {

    public static void main(String[] args) {

        String[] uchenik = new String[18];

        uchenik[0] = "Вася1";
        uchenik[1] = "Зина1";
        uchenik[2] = "Коля1"; //3
        uchenik[3] = "Аня1";
        uchenik[4] = "Степан1";     //5
        uchenik[5] = "Коля2"; //3
        uchenik[6] = "Катя1";
        uchenik[7] = "Степан2";
        uchenik[8] = "Вася2"; //3
        uchenik[9] = "Вова1";        //5
        uchenik[10] = "Зоя1";
        uchenik[11] = "Аня2"; //3
        uchenik[12] = "Зина2";
        uchenik[13] = "Вася3";
        uchenik[14] = "Гена1";    ///5
        uchenik[15] = "Саша1";
        uchenik[16] = "Гриша1";
        uchenik[17] = "Люба1";

        int uch3 = 0;
        int x = 0;

        //Каждый третий
        uch3 = 3;
        String[] x3 = new String[uchenik.length / uch3];
        System.out.println("Каждый третий:");

        for (int i = 0; i < uchenik.length; i++) {
            if (i != 0 & i % uch3 == 0) {
                x3[x] = uchenik[i - 1];
                System.out.println(x3[x]);
            }

        }
        System.out.println();
        //Каждый пятый
        uch3 = 5;

        System.out.println("Каждый пятый:");

        for (int i = 0; i < uchenik.length; i++) {
            if (i != 0 & i % uch3 == 0) {
                x3[x] = uchenik[i - 1];
                System.out.println(x3[x]);
            }

        }

    }


}
