package archive;

public class LearningDay1 {
    public static void main(String[] args) {

        int year = 1999;
        int dayNum = 91;

        System.out.print("1__");
        date(year, dayNum);

        System.out.print("2__");
        date(2000 , 91);
        System.out.print("3__");
        date(1999 , 186);
        System.out.print("4__");
        date(2000 , 186);
        System.out.print("5__");
        date(1999 , 304);
        System.out.print("6__");
        date(2000 , 304);
        System.out.print("7__");
        date(1999 , 309);
        System.out.print("8__");
        date(2000 , 309);
        System.out.print("9__");
        date(1999 , 327);

    }

    public static void date(int year, int dayNum) {
        System.out.print(year + " " + dayNum + " ");
        int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] monthName = {"January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December", "Months"};
        int monthNum = 0;
        boolean isLeapYear = (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);

        for (int days : daysInMonths) {
            if (isLeapYear == true && monthNum == 1) {
                days++;
            }
            if (dayNum > days) {
                dayNum -= days;
                monthNum++;
            } else {
                break;
            }
        }
        System.out.println(monthName[monthNum] + " " + dayNum);
    }
}
