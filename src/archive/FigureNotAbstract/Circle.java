package archive.FigureNotAbstract;


public class Circle implements Figure {
    private int radius;

    Circle(int radius){
        this.radius = radius;
    }

    @Override
    public void calcArea(){
        System.out.println("circle area is: ");
        System.out.println(Math.PI*(Math.pow(radius, 2)));
    }
}
