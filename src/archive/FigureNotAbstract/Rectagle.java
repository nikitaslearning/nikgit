package archive.FigureNotAbstract;

/**
 * Created by n.bashkin on 01.02.2018.
 */
public class Rectagle implements Figure {
    int side1;
    int side2;

    Rectagle(int side1, int side2){
        this.side1 = side1;
        this.side2 = side2;
    }

    public void calcArea(){
        System.out.println("rectagle area is: ");
        System.out.println(side1 * side2);
    }
}
