package archive.FigureNotAbstract;

/**
 * Created by n.bashkin on 01.02.2018.
 */
public class Square implements Figure{
    int side;

    Square(int side){
        this.side = side;
    }

    public void calcArea(){
        System.out.println("square area is: ");
        System.out.println(Math.pow(side, 2));
    }
}
