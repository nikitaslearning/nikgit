package archive.FigureNotAbstract;


public class Triangle implements Figure {
    private int side1;
    private int side2;
    private int side3;

    Triangle(int side1, int side2, int side3){
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public void calcArea(){

        double p = (side1 + side2 + side3) / 2;
        System.out.println("triangle area is: ");
        System.out.println(Math.sqrt(p*(p - side1)*(p - side2)*(p - side3)));
    }
}
