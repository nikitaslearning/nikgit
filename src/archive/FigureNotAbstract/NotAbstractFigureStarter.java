package archive.FigureNotAbstract;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class NotAbstractFigureStarter {
    public static void main(String[] args) throws IOException {


        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream("C:\\Users\\Чёрнаямолния\\Desktop\\Новый текстовый документ (2).txt"));

            AddDeleteReplaceFigureDataManager figureDataManager = new AddDeleteReplaceFigureDataManager();

            while (dataInputStream.available() > 0) {
                String line = dataInputStream.readLine();
                int space = line.indexOf(" ");
                String key = line.substring(0, space);
                int start, end;
                start = space + 1;
                end = line.indexOf(" ", start);

                switch (key) {
                    case "circle":
                        int r;
                        key = line.substring(start);
                        r = Integer.parseInt(key);
                        Figure circle = new Circle(r);
                        figureDataManager.addFigure(circle);
                        break;
                    case "rectagle":
                        int sr1, sr2;
                        key = line.substring(start, end);
                        sr1 = Integer.parseInt(key);
                        start = end + 1;
                        key = line.substring(start);
                        sr2 = Integer.parseInt(key);
                        Rectagle rectagle = new Rectagle(sr1, sr2);
                        figureDataManager.addFigure(rectagle);
                        break;
                    case "square":
                        int ss;
                        key = line.substring(start);
                        ss = Integer.parseInt(key);
                        Square square = new Square(ss);
                        figureDataManager.addFigure(square);
                        break;
                    case "triangle":
                        int st1, st2, st3;
                        key = line.substring(start, end);
                        st1 = Integer.parseInt(key);
                        start = end + 1;
                        end = line.indexOf(" ", start);
                        key = line.substring(start, end);
                        st2 = Integer.parseInt(key);
                        start = end + 1;
                        key = line.substring(start);
                        st3 = Integer.parseInt(key);
                        Triangle triangle = new Triangle(st1, st2, st3);
                        figureDataManager.addFigure(triangle);
                        break;
                    default:
                        System.out.println("unknown figure");
                        break;
                }
            }

            for (Figure f : figureDataManager.getFiguresData()) {
                f.calcArea();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
