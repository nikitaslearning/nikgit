package archive.FigureNotAbstract;


public class AddDeleteReplaceFigureDataManager {

    private Figure[] figuresData = new Figure[0];

    public Figure[] getFiguresData() {
        return figuresData;
    }

    public void addFigure(Figure figure){
        if (figuresData.length == 0){
            figuresData = new Figure[]{figure};
        }else {
            Figure[] buffer = figuresData;
            figuresData = new Figure[buffer.length + 1];
            System.arraycopy(buffer,0,figuresData,0, buffer.length);
            figuresData[buffer.length] = figure;
        }
    }
}
