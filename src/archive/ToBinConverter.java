package archive;

public class ToBinConverter {
    public static void main(String[] args) {

        int num =55327;
        System.out.println(getBinary(num));
    }
    public static String getBinary(int num){
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 32; i++){
            sb.append(num&1);
            if (i == 7 || i == 15 || i == 23) {
                sb.append(" ");
            }
            num>>=1;
        }
        sb.reverse();
        return sb.toString();
    }
}



//        int ii=-0b10;
//
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii),ii);
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii>>1),ii>>1);
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii>>>1),ii>>>1);
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii<<1),ii<<1);
//
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii),ii);
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii>>2),ii>>2);
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii>>>2),ii>>>2);
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii<<2),ii<<2);
//        int ii=0b10101;
//        int mask=0b10;
//
//        System.out.format("%32s=%d\n",Integer.toBinaryString(ii&mask),ii&mask);