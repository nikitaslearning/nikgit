package archive;

import enc.StringEncoder;
import java.io.*;

public class TextReader {

    public static void main(String[] args) {

        String pathForRead = "c:\\Users\\n.bashkin\\Desktop\\test3sicret.txt";

        try {
            DataInputStream dim = new DataInputStream(new FileInputStream(pathForRead));
            byte[] b = new byte[dim.available()];

            for (int i = 0; dim.available() > 0; i++) {
                b[i] = dim.readByte();
            }

            StringEncoder se = new StringEncoder();
            se.strEncoder(b, "sicret");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

