package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 * <p>
 * Задача 14
 * <p>
 * Отсортировать массивы из пред задачи методом пузырька
 * Суть пузырька - меняем местами два элемента рядом, если они
 * стоят не по возрастанию
 * <p>
 * Повторять до тех пор пока не будет цикль по массиву который
 * не внесет изменений (т.е.  все стоят верно)
 * повторять до тех пор
 * есть специальный цикл While  do {
 * <p>
 * } While (были_изменения==true)
 */
public class Start14 {

    public static void main(String[] args) {

        int spisok[] = new int[9];

        spisok[0] = 59;
        spisok[1] = 5;
        spisok[2] = 1100;
        spisok[3] = 12;
        spisok[4] = 223;
        spisok[5] = 14;
        spisok[6] = 12;
        spisok[7] = 304;
        spisok[8] = 1;

        boolean end = false;

        do {
            end = true;
            for (int i = 0; i < spisok.length - 1; i++) {

                if (spisok[i] > spisok[i + 1]) {
                    int x = spisok[i];
                    spisok[i] = spisok[i + 1];
                    spisok[i + 1] = x;
                    end = false;
                }
            }
        }
        while (end == false);

        for (int print:spisok) {
            System.out.println(print);
        }

    }
}