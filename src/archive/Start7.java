package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 * <p>
 * дан прямоугольник
 * заданы координаты (x1,y1) левого верхнего,
 * и правого нижнего угла (x2,y2)
 * найти площадь прямоугольника
 */
public class Start7 {

    public static void main(String[] args) {

        int x1 = 15;
        int y1 = 10;
        int x2 = 40;
        int y2 = 60;

        int res = (x2 - x1) * (y2 - y1);

        System.out.println(res);


    }
}