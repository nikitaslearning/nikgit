package archive;

/**
 * Created by n.bashkin on 14.06.2017.
 */
public class RotationArreySwap {


    public static void main(String[] args) {

        int k = 1000000;
        int n = 3000;

        int[] dano = new int[k];
// Print
//            for (int i = 0; i < dano.length; i++) {
//                dano[i] = i;
//                System.out.print(dano[i]);
//            }


        dano = Rotation(dano, n);
// Print
//            System.out.println();
//            for (int i = 0; i < dano.length; i++) {
//                System.out.print(dano[i]);
//            }
    }

    static int[] Rotation(int[] a, int b) {

        for (int i = 0; i < b; i++) {
            int tail = a[a.length - 1];
            for (int i1 = a.length - 1; i1 > 0; i1--) {

                a[i1] = a[i1 - 1];

            }
            a[0] = tail;
        }
        return a;
    }
}
