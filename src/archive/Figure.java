package archive;


abstract class Figure {

    abstract int[] getPoints();

    abstract String getFegureType();

    void printName() {
        String name = getFegureType();
        System.out.println("Эта фигура типа: " + name);
    }
}

class Circle extends Figure {
    int[] points = new int[1];

    Circle(int point1) {
        points[0] = point1;
    }

    @Override
    int[] getPoints() {
        return points;
    }


    @Override
    String getFegureType() {
        return "archive.Circle";
    }
}

class Rectangle extends Figure {

    int[] points = new int[4];

    Rectangle(int point1, int point2, int point3, int point4) {
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;
        points[3] = point4;

    }

    @Override
    int[] getPoints() {
        return points;
    }

    @Override
    String getFegureType() {
        return "Rectagle";
    }
}

class Rhomb extends Figure {

    int[] points = new int[4];

    Rhomb(int point1, int point2, int point3, int point4) {
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;
        points[3] = point4;
    }

    @Override
    String getFegureType() {
        return "archive.Rhomb";
    }

    @Override
    int[] getPoints() {
        return points;
    }
}