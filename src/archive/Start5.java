package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 *
 *   Задание 5.
 *   Тоже самое что 4, тока найти минимальное значение в массиве
 *
 */
public class Start5 {

    public static void main(String[] args) {

        int chisla[];
        chisla = new int[7];

        chisla[0] = 150;
        chisla[1] = 20;
        chisla[2] = 41;
        chisla[3] = 40;
        chisla[4] = 50;
        chisla[5] = 6;
        chisla[6] = 7000;


        int bignew = chisla[0];

        for (int i = 0; i < chisla.length; i++) {

            if (chisla[i] < bignew) {
                bignew = chisla[i];
            }

        }

        System.out.println(bignew);
    }
}