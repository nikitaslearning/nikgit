package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 * <p>
 * Задача 15
 * <p>
 * Придумайте способ превращения числа, в массив из его
 * разрядов.  Пример: 562 -> [5,6,2].
 */
public class Start15_my_exp {

    public static void main(String[] args) {

        int chislo = 2889516;
        int i = 0;

        int spisok[] = new int[10];
        while (chislo > 0) {
            int cut = chislo / 10;
            spisok[i] = chislo - cut * 10;
            chislo = cut;
            i++;
        }
        int x[] = new int[i];

        for (int res = 0; res < x.length; res++) {
            x[res] = spisok[i-1];
            i--;
            System.out.print(x[res]);
        }
    }
}