package archive;

import javax.swing.*;
import java.io.*;

/**
 * Created by Чёрнаямолния on 31.05.2017.
 */
public class FileRecord {

    public static void main(String[] args) {
        File file = new File("e:");
        File[] files = file.listFiles();

/*
        try {
            FileOutputStream myFile = new FileOutputStream("E:\\test");
            myFile.write(50);
            myFile.write(50);
            myFile.write(32);
            myFile.write(50);
            myFile.write(50);
            myFile.close();


        } catch (FileNotFoundException e) {
            System.out.println("не вып");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("не вып");
            e.printStackTrace();
        }
*/
        try {
            FileWriter myTextFile = new FileWriter("E:\\myTextFile.txt");
            myTextFile.write("Hello\n");
            myTextFile.write("Hello");
            myTextFile.close();

        } catch (IOException e) {
            System.out.println("Text file not write");
            e.printStackTrace();
        }

        try {
            FileReader fileReader = new FileReader("e:\\myTextFile.txt");
            int bukva;
            while ((bukva=fileReader.read())!=-1){

                System.out.print((char) bukva);
            }

        } catch (FileNotFoundException e) {
            System.out.println("file can't read");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
