package archive;

import java.io.*;
import java.util.*;

public class Tester {

    /**
     * Complete the function below.
     * DONOT MODIFY anything outside this function!
     */

/*
дополнить rearrange, больше никуда нельзя добавлять код

вернуть массив, отсортированный по приниципу
сначала идут числа у которых в двоичном виде больше единиц
если у числед оцинаково единиц в двоично м виде то сортировать по возврастанию.
*/
    static int[] rearrange(int[] elements) {

        Integer[] elementsObj = new Integer[elements.length];
        for (int i = 0; i < elementsObj.length; i++) {
            elementsObj[i] = new Integer(elements[i]);
        }

        Comparator<Integer> trueBitAndValueComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int flag = Integer.bitCount(o1) - Integer.bitCount(o2);

                if (flag == 0) {
                    flag = o1.intValue() - o2.intValue();
                }
                return flag;
            }
        };

        Arrays.sort(elementsObj, trueBitAndValueComparator);

        for (int i = 0; i < elementsObj.length; i++) {
            elements[i] = elementsObj[i].intValue();
        }
        return elements;
    }

    /**
     * DO NOT MODIFY THIS METHOD!
     */
    public static void main(String[] args) throws IOException {
//        Scanner in = new Scanner(System.in);
        int[] res;

//        int _elements_size = 0;
//        _elements_size = Integer.parseInt(in.nextLine().trim());
        int[] _elements = {
                5,
                3,
                7,
                10,
                14};
        int _elements_item;
//        for(int _elements_i = 0; _elements_i < _elements_size; _elements_i++) {
//            _elements_item = Integer.parseInt(in.nextLine().trim());
//            _elements[_elements_i] = _elements_item;
//        }

        res = rearrange(_elements);
        for (int res_i = 0; res_i < res.length; res_i++) {
            System.out.println(String.valueOf(res[res_i]));
        }
    }
}

/*
* Sample Input p1

5
5
3
7
10
14

Sample Output p1

3
5
10
7
14
*
* */

/*
       boolean run = true;
        int buffer;

        while (run) {
            run = false;
            for (int i = 0; i < elements.length - 1; i++) {
                if (Integer.bitCount(elements[i]) > Integer.bitCount(elements[i + 1])) {
                    buffer = elements[i];
                    elements[i] = elements[i + 1];
                    elements[i + 1] = buffer;
                    run = true;
                } else {
                    if (Integer.bitCount(elements[i]) == Integer.bitCount(elements[i + 1])) {
                        if (elements[i] > elements[i + 1]) {
                            buffer = elements[i];
                            elements[i] = elements[i + 1];
                            elements[i + 1] = buffer;
                            run = true;
                        }
                    }
                }
            }
        }

        for (
                int y : elements)

        {
            System.out.println("bits---" + Integer.bitCount(y));
        }
 */