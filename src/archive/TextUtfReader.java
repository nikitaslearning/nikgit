package archive;

import enc.StringEncoder;

import java.io.*;


public class TextUtfReader {
    public static void main(String[] args) {

        String pathRead = "C:\\Users\\Чёрнаямолния\\Desktop\\Gen3\\test2[win1251].txt";


        try {
            DataInputStream di = new DataInputStream(new FileInputStream(pathRead));

            byte[] b = new byte[di.available()];
            StringBuilder sb = new StringBuilder();
            int c = 0;
            for (int i = 0; di.available() > 0; i++) {

                b[i] = di.readByte();

                if (b[i] != 13) {
                    sb.append((char) b[i]);
                } else {
                    int v = Integer.parseInt(sb.toString()) - 128;
                    sb.delete(0, sb.length());
                    b[c] = (byte) v;
                    c++;
                    di.readByte();
                }
                if (di.available() == 0) {
                    int v = Integer.parseInt(sb.toString()) - 128;
                    b[c] = (byte) v;
                }
            }
            byte[] b1 = new byte[c + 1];
            for (int i = 0; i < b1.length; i++) {
                b1[i] = b[i];
            }


            StringEncoder se = new StringEncoder();
            se.strEncoder(b1, encName(pathRead));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

  static String encName(String pathName) {

        char[] c = pathName.toCharArray();
        StringBuilder sb = new StringBuilder();

        for (int i = c.length - 1; i > 0; i--) {

            if (c[i] == ']') {
                i--;
                while (c[i] != '[') {
                    sb.append(c[i]);
                    i--;
                }
                break;
            }
        }
        sb.reverse();
        pathName = sb.toString();

        return pathName;
    }
}