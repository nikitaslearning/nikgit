package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 * <p>
 * Задача 13
 * Найдите победителя марафона.
 * Группа людей участвует в марафоне, их имена и время за которое они пробежали марафон вы можете увидеть ниже.
 * Ваша задача найти человека, который быстрее всех пробежал дистанцию и вывести его имя и счет.
 * <p>
 * String[] names = { "Elena", "Thomas", "Hamilton", "Suzie", "Phil", "Matt", "Alex", "Emma", "John",
 * "James", "Jane", "Emily", "Daniel", "Neda", "Aaron", "Kate" };
 * <p>
 * int[] times = { 341, 273, 278, 329, 445, 402, 388, 275, 243, 334, 412, 393, 299, 343, 317, 265 }
 */
public class Start13 {

    public static void main(String[] args) {

        String[] names = {"Elena", "Thomas", "Hamilton", "Suzie", "Phil", "Matt", "Alex", "Emma", "John", "James",
                "Jane", "Emily", "Daniel", "Neda", "Aaron", "Kate"};
        int[] times = {341, 273, 278, 329, 445, 402, 388, 275, 243, 334, 412, 393, 299, 343, 317, 265};

        int x = 0;
        int res = 0;

        for (int i = 0; i < times.length; i++) {

            if (x < times[i]) {
                x = times[i];
                res=i;
            }
        }
        System.out.println(names[res]+" "+ times[res]);


    }


}
