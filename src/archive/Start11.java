package archive;

/**
 * Created by Чёрнаямолния on 13.02.2017.
 * <p>
 * Задание 10
 * дано
 * x - количество квартир на этаже
 * y - количество этажей.
 * нужно заполнить массив kvart номер квартиры = этаж где она находится.
 * т.е. получим массив справочник на каком этажде расположена квартира.
 * индекс элемента - номер квартиры
 * значение элемента - этаж
 * <p>
 * ну например будет
 * kvart[1]=1 // квартира 1 на первом этаже
 * kvart[2]=1 // квартира 2 на первом этаже
 * kvart[20]=4 // квартира 20 на 4 этаже
 * <p>
 * в 10 нужен "остаток от деления"
 */
public class Start11 {

    public static void main(String[] args) {


        int x = 2;
        int y = 5;
        int[] kvart;
        kvart = new int[x * y];
        int level = 0;

        for (int i = 0; i < kvart.length; i++) {

            if (i % x == 0) {
                level++;
            }
            kvart[i] = level;
            System.out.println("квартира " + (i+1) + " этаж " + kvart[i]);

        }


    }
}