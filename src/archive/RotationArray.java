package archive;

/**
 * Created by n.bashkin on 09.06.2017.
 */
public class RotationArray {

    public static void main(String[] args) {

        int k = 13;
        int n = 3;

        int[] dano = new int[k];

        for (int i = 0; i < dano.length; i++) {
            dano[i] = i + 1;
        }

        RotationArray rA = new RotationArray();

        dano = rA.rotation3(dano, n);

    }

    int[] rotation3(int[] dano, int n) {
        if (n > dano.length) {
            n %= dano.length;
        }
        int s = 0;

        for (int i = 0; i < dano.length; i++) {

            int save = dano[s + n];
            s += n;
            dano[s + n] = save;

        }
        return dano;
    }

    int[] rotation(int[] a, int n) {
        if (a.length < n) {
            n %= a.length;
        }
        int[] b = new int[a.length];
        int y = 0;

        for (int i = 0; i < a.length; i++) {

            if (y + n > b.length - 1) {
                y = 0 - (a.length - i);
            }

            b[y + n] = a[i];

            y++;
        }
        return b;
    }
}
