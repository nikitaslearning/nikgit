package archive;

public class ArrayOfNumbers {

    int[] arrayOfNumbers;


    int[] get() {
        return arrayOfNumbers;
    }

    void add(int add) {

        if (arrayOfNumbers == null) {
            arrayOfNumbers = new int[1];
            arrayOfNumbers[arrayOfNumbers.length - 1] = add;
        } else {
            int[] vrem = new int[arrayOfNumbers.length + 1];
            System.arraycopy(arrayOfNumbers, 0, vrem, 0, arrayOfNumbers.length);
            arrayOfNumbers = new int[arrayOfNumbers.length + 1];
            arrayOfNumbers = vrem;
            arrayOfNumbers[arrayOfNumbers.length - 1] = add;
        }
    }
}
