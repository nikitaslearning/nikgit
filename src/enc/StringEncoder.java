package enc;




public class StringEncoder {



    public void strEncoder(byte[] b, String encName) {

        switch (encName) {
            case "win1251":
                win1251(b);
                break;
            case "sicret":
                sicret(b);
                break;

            default:
                System.out.println("Unknown format");
                break;
        }
    }

   static void win1251(byte[] b) {

        for (int i = 0; i < b.length; i++) {

            int symb = b[i] + 128;

            switch (symb) {

                case 10:
                    System.out.print("\n");
                    break;
                case 13:
                    System.out.print("\r");
                    break;
                case 32:
                    System.out.print(" ");
                    break;
                case 48:
                    System.out.print("0");
                    break;
                case 49:
                    System.out.print("1");
                    break;
                case 50:
                    System.out.print("2");
                    break;
                case 51:
                    System.out.print("3");
                    break;
                case 52:
                    System.out.print("4");
                    break;
                case 53:
                    System.out.print("5");
                    break;
                case 54:
                    System.out.print("6");
                    break;
                case 55:
                    System.out.print("7");
                    break;
                case 56:
                    System.out.print("8");
                    break;
                case 57:
                    System.out.print("9");
                    break;
                case 224:
                    System.out.print("а");
                    break;
                case 225:
                    System.out.print("б");
                    break;
                case 226:
                    System.out.print("в");
                    break;
                case 227:
                    System.out.print("г");
                    break;
                case 228:
                    System.out.print("д");
                    break;
                case 229:
                    System.out.print("е");
                    break;
                case 230:
                    System.out.print("ж");
                    break;
                case 231:
                    System.out.print("з");
                    break;
                case 232:
                    System.out.print("и");
                    break;
                case 233:
                    System.out.print("й");
                    break;
                case 234:
                    System.out.print("к");
                    break;
                case 235:
                    System.out.print("л");
                    break;
                case 236:
                    System.out.print("м");
                    break;
                case 237:
                    System.out.print("н");
                    break;
                case 238:
                    System.out.print("о");
                    break;
                case 239:
                    System.out.print("п");
                    break;
                case 240:
                    System.out.print("р");
                    break;
                case 241:
                    System.out.print("с");
                    break;
                case 242:
                    System.out.print("т");
                    break;
                case 243:
                    System.out.print("у");
                    break;
                case 244:
                    System.out.print("ф");
                    break;
                case 245:
                    System.out.print("х");
                    break;
                case 246:
                    System.out.print("ц");
                    break;
                case 247:
                    System.out.print("ч");
                    break;
                case 248:
                    System.out.print("ш");
                    break;
                case 249:
                    System.out.print("щ");
                    break;
                case 250:
                    System.out.print("ъ");
                    break;
                case 251:
                    System.out.print("ы");
                    break;
                case 252:
                    System.out.print("ь");
                    break;
                case 253:
                    System.out.print("э");
                    break;
                case 254:
                    System.out.print("ю");
                    break;
                case 255:
                    System.out.print("я");
                    break;
                default:
                    System.out.print("?");
                    break;
            }
        }
    }

   static void sicret(byte[] b) {

        for (int i = 0; i < b.length; i++) {

            switch (b[i]) {

                case 10:
                    System.out.print(".");
                    break;
                case 32:
                    System.out.print("пробел");
                    break;
                case 102:
                    System.out.print("⛄");
                    break;
                case 121:
                    System.out.print("⼽");
                    break;
                case 13:
                    System.out.print("Ⅷ");
                    break;
                default:
                    System.out.print("?");
                    break;
            }
        }
    }

}

